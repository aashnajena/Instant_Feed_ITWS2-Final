from flask import *
import Utility as db
import newspaper
from newspaper import Article
from scrape import func
import datetime 
import Search_Bar as Search
d=func()
myarts = db.retrieveArticles(request,"allallall")
glob = []
for item in d:
	for i in range(1,5):
		url=d[item][i][1]
		article=Article(url)
		article.download()
		article.parse()
		article.nlp()
		d[item][i].append(article.title)
		d[item][i].append(article.summary)
		d[item][i].append(article.keywords)
		d[item][i].append(article.publish_date)


app = Flask(__name__)
app.secret_key = 'MKhJHJH798798kjhkjhkjGHh'

@app.route('/',methods=['GET','POST'])
def main():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		return render_template('new_index.html',logged_in=True, username=session['username'],message=None,**d, isAdmin = str(isAdmin[0]))
	else:
		return render_template('new_index.html', logged_in = False, username=None,message=None,**d, isAdmin = '0')

@app.route('/signin',methods=['POST','GET'])
def signin():
	if request.method=='POST':
		next = request.values.get('next')
		#print ("password looks like", request.values.get('psw'))
		status,usr = db.authenticate(request)
		if (status):
			if not next:
				global tup 
				tup = db.preferences(request, request.form['uname'])
				global lis 
				lis = tup[0].split(',')
				#print(lis)
				#return #render_template("new_homepage.html", message = session['username'],dic=d,lis=lis)
				return redirect('/homepage')
			else:
				print  ("On redirect - next=", next)
				return redirect(next)
		else:
			return render_template("signin.html", message="Incorrect username or password. Try to login again.",**d)
	else:	
		if ('username' in session):
			return render_template("new_index.html", username=session['username'],message=session['username'] + " is already logged in. Log out to proceed.",**d)
		else:
			return render_template('signin.html', message="Please Login")

@app.route('/signout', methods=['POST', 'GET'])
def signout():
	if 'username' in session:
		name = session.pop('username')
		return render_template("new_index.html", message=name +" has logged out Successfully.",**d)
	return render_template("new_index.html", message="You are already logged out.",**d)

@app.route('/signup',methods=['POST','GET'])
def func():
	if request.method == 'POST':
		return db.insertUser(request)
	else:
		return render_template('new_signup.html')

@app.route('/sports')
def sports():
	myarts = db.retrieveArticles(request,"allallall")
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		return render_template('sports.html', message=session['username'],**d, arts=myarts, isAdmin = str(isAdmin[0]))	
	else:
		return render_template('sports.html', message=None,**d, arts=myarts, isAdmin = '0') 	

@app.route('/world')
def world():
	myarts = db.retrieveArticles(request,"allallall")
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		return render_template('world.html', message=session['username'],**d, arts=myarts, isAdmin = str(isAdmin[0]))	
	else:
		return render_template('world.html', message=None,**d, arts=myarts, isAdmin = '0')

@app.route('/ent')
def ent():
	myarts = db.retrieveArticles(request,"allallall")
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		return render_template('ent.html', message=session['username'],**d, arts=myarts,isAdmin = str(isAdmin[0]))	
	else:
		return render_template('ent.html', message=None,**d, arts=myarts,isAdmin = '0')

@app.route('/tech')
def tech():
	myarts = db.retrieveArticles(request,"allallall")
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		return render_template('tech.html', message=session['username'],**d, arts=myarts, isAdmin = str(isAdmin[0]))	
	else:
		return render_template('tech.html', message=None,**d,arts=myarts, isAdmin = '0')	


@app.route('/homepage')
def homepage():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		global tupp 
		tupp = db.preferences(request, session['username'])
		global liss 
		liss = tupp[0].split(',')
		myarts = db.retrieveArticles(request,"allallall")
		return render_template('new_homepage.html', message=session['username'],dic=d,lis=liss, isAdmin=str(isAdmin[0]), arts=myarts)	
	else:
		return redirect('/signin')

@app.route('/searchResults',  methods=['POST', 'GET'])
def search():
	if request.method=='POST':
		next = request.values.get('next')
		query = request.form['search']
		retlist = Search.func(query,d)
		myarts = db.retrieveArticles(request,"allallall")
		selfarticles = Search.selfarticles(query, myarts)
		print(selfarticles)


		if retlist==[] and selfarticles==[]:
			if 'username' in session:
				isAdmin = db.findadmin(request,session['username'])
				return render_template('searchresults.html', message=session['username'],search=retlist,mess="Sorry, no search results found.",word=query, isAdmin=str(isAdmin[0]))
			else:
				return render_template('searchresults.html', message=None,search=retlist,mess="Sorry, no search results found.",word=query,isAdmin='0')

		else:			
			if 'username' in session:
				isAdmin = db.findadmin(request,session['username'])
				return render_template('searchresults.html', message=session['username'],search=retlist,word=query,mess=None,isAdmin=str(isAdmin[0]),selfarts = selfarticles)
			else:
				return render_template('searchresults.html', message=None,search=retlist,word=query,mess=None,isAdmin='0',selfarts=selfarticles)
	else:
		if 'username' in session:
			isAdmin = db.findadmin(request,session['username'])
			return render_template('new_index.html',logged_in=True, username=session['username'],message="No results found",**d, isAdmin=str(isAdmin[0]))
		else:
			return render_template('new_index.html', logged_in = False, username=None,message="No results found",**d, isAdmin='0')
@app.route('/admin',methods=['POST','GET'])
def write():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		if request.method == 'POST':
			return db.insertArticle(request,session['username'])
		else:
			return render_template('admin.html',message=session['username'],isAdmin=str(isAdmin[0]))
	else:
		return redirect('/signin')

@app.route('/myarticles',methods=['POST','GET'])
def myarticles():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		row = db.retrieveArticles(request,session['username'])	
		return render_template('myarticles.html', l = row,message=session['username'],isAdmin=str(isAdmin[0]))
	else:
		return redirect('/signin')

@app.route('/delete',methods=['POST','GET'])
def delete():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		if request.method == 'POST':
			title = request.args['title']
			print(title)
			db.deleteArticle(request,title)	
			return redirect('/myarticles')
		else:
			return redirect('/myarticles')
	else:
		return redirect('/signin')	

@app.route('/changeprofile',methods=['POST','GET'])
def change():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		if request.method == 'POST':
			changewhat = request.args['change']
			if changewhat == "news":
				db.changepreference(request,session['username'])
				return redirect('/')
		else:
			return render_template("changeprofile.html",message=session['username'],isAdmin=str(isAdmin[0]))
	else:
		return redirect('/signin')

@app.route('/read',methods=['POST','GET'])
def read():
	if 'username' in session:
		isAdmin = db.findadmin(request,session['username'])
		if request.method == 'POST':
			title = request.args['title']
			timestamp = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
			db.addComment(request,title,session['username'],timestamp)
			return redirect('/read?title='+title)
		else:
			title = request.args['title']
			row = db.retrieveArticle(request,title)
			comments = db.retrieveComments(request,title)
			author = db.retrieveAuthor(request,title)
			print (author)
			return render_template("articles.html",message=session['username'],i=row,isAdmin=str(isAdmin[0]),comments=comments, author=author[0])
	else:
		return redirect('/signin')	

@app.route('/deleteComment',methods=['POST','GET'])
def deleteComment():	
	isAdmin = db.findadmin(request,session['username'])
	timee = request.args['time']
	timee = str(timee)
	if request.method == 'POST':
		msg = db.deleteComment(request,timee)
		return redirect('/homepage')
	else:
		return redirect('/homepage')	


if __name__ == "__main__":
	app.run(debug=True)