import sqlite3 as sql
from flask import flash, render_template, redirect, session
from passlib.hash import sha256_crypt
import datetime
import time

def insertUser(request):
	connect = sql.connect("database.db")
	current = connect.cursor()
	username = request.form['username']
	password = request.form['password']
	password_ = request.form['confirm']
	access = request.form['access']
	print(access)
	if access == "admin":
		access = 1
	else:
		access = 0
	preference = request.form.getlist('preference')
	preference = ",".join(preference)
	#print(preference)
	Username_Check = "select username from users where (username = '" + username +"')"
	current.execute(Username_Check)
	row = current.fetchone()

	if row:
		connect.close()
		return render_template('new_signup.html',message="You are already logged out.")
	else:
		if password != password_:
			return render_template('new_signup.html',message="You are already logged out.")
		else:
			password = sha256_crypt.encrypt(password)
			current.execute("INSERT INTO users (preferences,username,password,access) values(?,?,?,?)",(preference,username,password,access))
			connect.commit()
			connect.close()
			return redirect('/')
def insertArticle(request,username):
	connect = sql.connect("database.db")
	current = connect.cursor()
	user = username
	topic = request.form['topic']
	title = request.form['title']
	content = request.form['article']
	img_link = request.form['img']
	summary = request.form['summary']
	print(username)
	current.execute("INSERT INTO articles (username,topic,title,content,imglink,summary) values(?,?,?,?,?,?)",(username,topic,title,content,img_link,summary))
	connect.commit()
	connect.close()
	return redirect('/')

def retrieveUsers(username):
	connect = sql.connect("database.db")
	current = connect.cursor()
	current.execute("SELECT * FROM users where username="+user+';')
	users = current.fetchall()
	connect.close()
	return users


def authenticate(request):
    connect = sql.connect("database.db")
    username = request.form['uname']
    password = request.form['psw']
    #print(username)
    sqlQuery = "select password from users where username = '%s'"%username
    cursor = connect.cursor()
    #print ("sqlQuery= "+ sqlQuery)
    cursor.execute(sqlQuery)
    row = cursor.fetchone()
    status = False
    if row:
        status = sha256_crypt.verify(password, row[0])
        #print ("password=",row[0]," status=",status)
        if status:
           msg = username + " has logged in Successfully."
           session['username'] = username
    else:
        msg = username + " has failed to login."

    return {status,username}

def preferences(request, user):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	preference_query = "select preferences from users where username= '%s'"%user
	cursor.execute(preference_query)
	row = cursor.fetchone()
	return row

def retrieveArticles(request,user):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	if user == "allallall":
		ArticleQuery = "select * from articles";
	else:
		ArticleQuery = "select * from articles where username = '" +user+ "'";		
	cursor.execute(ArticleQuery)
	row = cursor.fetchall()
	return row

def retrieveArticle(request,title):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	ArticleQuery = "select * from articles where title = '" +title+ "'";		
	cursor.execute(ArticleQuery)
	row = cursor.fetchone()
	return row

def deleteArticle(request,title):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	deleteQuery = "delete from articles where title = '"+title+"';"
	print(deleteQuery)
	cursor.execute(deleteQuery)
	connect.commit()
	return redirect('/myarticles')

def changepreference(request,user):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	preference = request.form.getlist('preference')
	preference = ",".join(preference)
	preference_query = "update users set preferences = '"+preference+"' where username = '"+user+"'"
	cursor.execute(preference_query)
	connect.commit()
	return redirect('/')

def addComment(request,title,user,timestamp):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	comment = request.form['comment']
	
	commentQuery = "insert into comments values('"+user+"','"+title+"','"+comment+"','"+timestamp+"');"
	cursor.execute(commentQuery)
	connect.commit()

def retrieveComments(request,title):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	query = "Select * from comments where title='"+title+"';"
	cursor.execute(query)
	comments = cursor.fetchall()
	return comments

def findadmin(request,username)	:
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	query = "select access from users where username='"+username+"';"
	cursor.execute(query)
	isAdmin = cursor.fetchone()
	return isAdmin

def retrieveAuthor(request,title):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	query = "Select username from articles where title='"+title+"';"
	cursor.execute(query)
	author = cursor.fetchone()
	return author	

def deleteComment(request,timee):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	print(timee)
	cursor.execute("delete from comments where time='"+timee+"';")
	connect.commit()
	msg = "done"
	return msg

def retrieveTitle(request,timee):
	connect = sql.connect("database.db")
	cursor = connect.cursor()
	query = "Select title from comments where time='"+timee+"';"
	cursor.execute(query)
	title = cursor.fetchone()
	return title			