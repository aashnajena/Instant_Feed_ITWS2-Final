def func(input,dict):
	input_from_search_bar = input
	output_list = []
	for category in dict:
		for item in dict[category]:
			flag=0
			for items in dict[category][item][4]:
				a = items.lower()
				if input.lower() == a:
					flag=1
					break
			if(flag==1):
				output_list.append(dict[category][item])
	return output_list

def selfarticles(input,ltuples):
	input_from_search_bar = input.lower()
	output_list = []
	for tuplee in ltuples:
		keywords = tuplee[2].split(" ")
		flag=0
		for key in keywords:
			lower_key = key.lower()
			if lower_key == input_from_search_bar:
				flag=1
				break

		if flag==1:
			flag=0
			output_list.append(tuplee)

	return output_list
